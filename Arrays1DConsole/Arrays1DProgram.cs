﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays1DConsole {

    internal class Arrays1DProgram {

        const double MIN = -10.51;
        const double MAX = 10.53;

        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            double[] doubles = initArray(inputValue());
            doubles = sorted(doubles);

            Console.Write("\nПерша половина масиву відсортована за зростанням:\n[");
            for (int i = 0; i < doubles.Length; i++) Console.Write((i > 0 ? "; " : "") + $"{doubles[i]}");
            Console.Write("]\n\n");

            Console.ReadLine();
        }

        static int inputValue() {
            Console.WriteLine("Вкажіть довжину масиву та натисніть Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                Console.WriteLine("Ви не ввели значення. Будь ласка, введіть значеня!!!\n");
                return inputValue();
            }

            try {
                int n = int.Parse(value);
                if (n > 0) return n;

                Console.WriteLine("Довжина масиву не може бути меншою за 1. Будь ласка, введіть значення ще раз!!!\n");
                return inputValue();
            } catch (System.Exception e) {
                Console.WriteLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, введіть значення ще раз!!!\n"
                );
                return inputValue();
            }

        }

        static double[] initArray(int size) {
            double[] doubles = new double[size];
            Random random = new Random();
            double sum = 0;

            Console.Write("\n[");
            for (int i = 0; i < size; i++) {
                double value = random.NextDouble() * (MAX - MIN) + MIN;
                double it = Math.Round(value, 2);
                doubles[i] = it;
                Console.Write((i > 0 ? "; " : "") + $"{it}");

                if (i > 0 && i % 3 == 0) sum += it;
            }
            Console.Write("]\n\n");

            Console.WriteLine(
                size < 4
                    ? "Масив не містить елементів, індекси яких діляться на 3"
                    : $"Сума елементів, індекс яких ділиться на 3: {sum:F2}"
            );

            return doubles;
        }

        static double[] sorted(double[] array) {
            int size = array.Length / 2;

            for (int i = 0; i < size; i++) {
                for (int j = 1; j < size - i; j++) {
                    if (array[j - 1] <= array[j]) continue;

                    double it = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = it;
                }
            }

            return array;
        }

    }
}
