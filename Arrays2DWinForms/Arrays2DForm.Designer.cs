﻿namespace Arrays2DWinForms
{
    partial class Arrays2DForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Arrays2DForm));
            this.maxSumTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.toSolveButton = new System.Windows.Forms.Button();
            this.generateButton = new System.Windows.Forms.Button();
            this.matrixDataGridView = new System.Windows.Forms.DataGridView();
            this.shiftButton = new System.Windows.Forms.Button();
            this.rowSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.colomnSizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.kNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.sumDataGridView = new System.Windows.Forms.DataGridView();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.matrixDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowSizeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.colomnSizeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sumDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // maxSumTextBox
            // 
            this.maxSumTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.maxSumTextBox.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.maxSumTextBox.Location = new System.Drawing.Point(200, 609);
            this.maxSumTextBox.Margin = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.maxSumTextBox.Name = "maxSumTextBox";
            this.maxSumTextBox.ReadOnly = true;
            this.maxSumTextBox.Size = new System.Drawing.Size(100, 29);
            this.maxSumTextBox.TabIndex = 15;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(33, 612);
            this.label2.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(155, 22);
            this.label2.TabIndex = 14;
            this.label2.Text = "Найбільша сума:";
            // 
            // toSolveButton
            // 
            this.toSolveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.toSolveButton.AutoSize = true;
            this.toSolveButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.toSolveButton.Enabled = false;
            this.toSolveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toSolveButton.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toSolveButton.ForeColor = System.Drawing.Color.White;
            this.toSolveButton.Location = new System.Drawing.Point(622, 604);
            this.toSolveButton.Margin = new System.Windows.Forms.Padding(0, 16, 16, 16);
            this.toSolveButton.Name = "toSolveButton";
            this.toSolveButton.Padding = new System.Windows.Forms.Padding(8, 2, 8, 2);
            this.toSolveButton.Size = new System.Drawing.Size(137, 38);
            this.toSolveButton.TabIndex = 13;
            this.toSolveButton.Text = "Розв’язати";
            this.toSolveButton.UseVisualStyleBackColor = false;
            this.toSolveButton.Click += new System.EventHandler(this.toSolveButtonClick);
            // 
            // generateButton
            // 
            this.generateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.generateButton.AutoSize = true;
            this.generateButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.generateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generateButton.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.generateButton.ForeColor = System.Drawing.Color.White;
            this.generateButton.Location = new System.Drawing.Point(622, 42);
            this.generateButton.Margin = new System.Windows.Forms.Padding(0, 16, 16, 0);
            this.generateButton.Name = "generateButton";
            this.generateButton.Padding = new System.Windows.Forms.Padding(8, 2, 8, 2);
            this.generateButton.Size = new System.Drawing.Size(137, 38);
            this.generateButton.TabIndex = 12;
            this.generateButton.Text = "Генерувати";
            this.generateButton.UseVisualStyleBackColor = false;
            this.generateButton.Click += new System.EventHandler(this.generateButtonClick);
            // 
            // matrixDataGridView
            // 
            this.matrixDataGridView.AllowUserToAddRows = false;
            this.matrixDataGridView.AllowUserToDeleteRows = false;
            this.matrixDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.matrixDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.matrixDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.matrixDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.matrixDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.matrixDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.matrixDataGridView.Location = new System.Drawing.Point(33, 115);
            this.matrixDataGridView.Margin = new System.Windows.Forms.Padding(16);
            this.matrixDataGridView.MinimumSize = new System.Drawing.Size(0, 64);
            this.matrixDataGridView.Name = "matrixDataGridView";
            this.matrixDataGridView.ReadOnly = true;
            this.matrixDataGridView.Size = new System.Drawing.Size(726, 247);
            this.matrixDataGridView.TabIndex = 11;
            this.matrixDataGridView.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.matrixDataGridViewCellPainting);
            // 
            // shiftButton
            // 
            this.shiftButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.shiftButton.AutoSize = true;
            this.shiftButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.shiftButton.Enabled = false;
            this.shiftButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.shiftButton.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.shiftButton.ForeColor = System.Drawing.Color.White;
            this.shiftButton.Location = new System.Drawing.Point(622, 432);
            this.shiftButton.Margin = new System.Windows.Forms.Padding(0, 16, 16, 16);
            this.shiftButton.Name = "shiftButton";
            this.shiftButton.Padding = new System.Windows.Forms.Padding(8, 2, 8, 2);
            this.shiftButton.Size = new System.Drawing.Size(137, 38);
            this.shiftButton.TabIndex = 10;
            this.shiftButton.Text = "Зсув";
            this.shiftButton.UseVisualStyleBackColor = false;
            this.shiftButton.Click += new System.EventHandler(this.shiftButtonClick);
            // 
            // rowSizeNumericUpDown
            // 
            this.rowSizeNumericUpDown.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.rowSizeNumericUpDown.Location = new System.Drawing.Point(228, 25);
            this.rowSizeNumericUpDown.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.rowSizeNumericUpDown.Name = "rowSizeNumericUpDown";
            this.rowSizeNumericUpDown.Size = new System.Drawing.Size(120, 29);
            this.rowSizeNumericUpDown.TabIndex = 9;
            this.rowSizeNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(33, 27);
            this.label1.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 22);
            this.label1.TabIndex = 8;
            this.label1.Text = "Кількість рядків";
            // 
            // colomnSizeNumericUpDown
            // 
            this.colomnSizeNumericUpDown.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.colomnSizeNumericUpDown.Location = new System.Drawing.Point(228, 70);
            this.colomnSizeNumericUpDown.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.colomnSizeNumericUpDown.Name = "colomnSizeNumericUpDown";
            this.colomnSizeNumericUpDown.Size = new System.Drawing.Size(120, 29);
            this.colomnSizeNumericUpDown.TabIndex = 17;
            this.colomnSizeNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(33, 72);
            this.label3.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(165, 22);
            this.label3.TabIndex = 16;
            this.label3.Text = "Кількість стовпців";
            // 
            // kNumericUpDown
            // 
            this.kNumericUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.kNumericUpDown.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.kNumericUpDown.Location = new System.Drawing.Point(84, 438);
            this.kNumericUpDown.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.kNumericUpDown.Name = "kNumericUpDown";
            this.kNumericUpDown.Size = new System.Drawing.Size(120, 29);
            this.kNumericUpDown.TabIndex = 19;
            this.kNumericUpDown.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(33, 440);
            this.label4.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 22);
            this.label4.TabIndex = 18;
            this.label4.Text = "k =";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(33, 394);
            this.label5.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(726, 22);
            this.label5.TabIndex = 20;
            this.label5.Text = "Зсув парних рядків матриці зліва направо на k позицій";
            // 
            // sumDataGridView
            // 
            this.sumDataGridView.AllowUserToAddRows = false;
            this.sumDataGridView.AllowUserToDeleteRows = false;
            this.sumDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sumDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.sumDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.sumDataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.sumDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.sumDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.sumDataGridView.Location = new System.Drawing.Point(33, 528);
            this.sumDataGridView.Margin = new System.Windows.Forms.Padding(16, 4, 16, 16);
            this.sumDataGridView.Name = "sumDataGridView";
            this.sumDataGridView.ReadOnly = true;
            this.sumDataGridView.Size = new System.Drawing.Size(726, 44);
            this.sumDataGridView.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(33, 502);
            this.label6.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(726, 22);
            this.label6.TabIndex = 22;
            this.label6.Text = "Сума елементів кожного рядка";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(543, 654);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(238, 22);
            this.label7.TabIndex = 23;
            this.label7.Text = "Ботвін О. Ю. гр. ЗІПЗ-22-1";
            // 
            // Arrays2DForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(784, 685);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.sumDataGridView);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.kNumericUpDown);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.colomnSizeNumericUpDown);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.maxSumTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.toSolveButton);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.matrixDataGridView);
            this.Controls.Add(this.shiftButton);
            this.Controls.Add(this.rowSizeNumericUpDown);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(728, 584);
            this.Name = "Arrays2DForm";
            this.Text = "Лабораторна робота No3. Завдання 2";
            ((System.ComponentModel.ISupportInitialize)(this.matrixDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rowSizeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.colomnSizeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sumDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox maxSumTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button toSolveButton;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.DataGridView matrixDataGridView;
        private System.Windows.Forms.Button shiftButton;
        private System.Windows.Forms.NumericUpDown rowSizeNumericUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown colomnSizeNumericUpDown;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown kNumericUpDown;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView sumDataGridView;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}

