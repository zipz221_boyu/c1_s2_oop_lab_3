﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays2DWinForms
{
    public partial class Arrays2DForm : Form {

        const double MIN = -110.34;
        const double MAX = 110.35;
        private double[,] matrix;

        public Arrays2DForm() {
            InitializeComponent();
        }

        private void generateButtonClick(object sender, EventArgs e) {
            buttonsEnable(false);
            matrixDataGridView.Rows.Clear();
            matrixDataGridView.Columns.Clear();
            sumDataGridView.Rows.Clear();
            sumDataGridView.Columns.Clear();
            maxSumTextBox.Text = "";

            int rowSize = (int) rowSizeNumericUpDown.Value;
            int colomnSize = (int) colomnSizeNumericUpDown.Value;

            matrix = new double[rowSize, colomnSize];

            if (rowSize < 1 || colomnSize < 1) {
                generateButton.Enabled = true;
                return;
            }

            Random random = new Random();
            matrixDataGridView.RowCount = rowSize;
            matrixDataGridView.ColumnCount = colomnSize;

            for (int i = 0; i < rowSize; i++) {
                matrixDataGridView.Rows[i].HeaderCell.Value = i.ToString();

                for (int j = 0; j < colomnSize; j++) {
                    double value = random.NextDouble() * (MAX - MIN) + MIN;
                    double it = Math.Round(value, 2);
                    matrix[i, j] = it;

                    matrixDataGridView[j, i].Value = it;
                    matrixDataGridView.Columns[j].HeaderText = j.ToString();
                    matrixDataGridView.Columns[j].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }

            generateButton.Enabled = true;
            shiftButton.Enabled = rowSize > 0 && colomnSize > 0;
            toSolveButton.Enabled = rowSize > 0 && colomnSize > 0;
        }

        private void shiftButtonClick(object sender, EventArgs e) {
            buttonsEnable(false);

            int k = (int) kNumericUpDown.Value;

            for (int row = 0; row < matrix.GetLength(0); row++) {
                if ((row + 1) % 2 != 0) continue;

                for (int i = 0; i < k; i++) {
                    int lastIndex = matrix.GetLength(1) - 1;
                    double lastItem = matrix[row, lastIndex];

                    for (int j = lastIndex; j > 0; j--) {
                        double it = matrix[row, j - 1];
                        matrix[row, j] = it;
                        matrixDataGridView[j, row].Value = it;
                    }

                    matrix[row, 0] = lastItem;
                    matrixDataGridView[0, row].Value = lastItem;
                }
            }

            buttonsEnable(true);
        }

        private void toSolveButtonClick(object sender, EventArgs e) {
            buttonsEnable(false);
            
            sumDataGridView.RowCount = 1;
            sumDataGridView.ColumnCount = matrix.GetLength(0);

            double maxSum = 0;
            for (int i = 0; i < matrix.GetLength(0); i++) {
                double sum = 0;
                for (int j = 0; j < matrix.GetLength(1); j++) sum = Math.Round(sum + matrix[i, j], 2);

                if (i == 0) maxSum = sum;
                else if (sum > maxSum) maxSum = sum;

                sumDataGridView[i, 0].Value = sum;
                sumDataGridView.Columns[i].HeaderText = i.ToString();
            }

            maxSumTextBox.Text = $"{maxSum:F2}";
            buttonsEnable(true);
        }

        private void matrixDataGridViewCellPainting(object sender, DataGridViewCellPaintingEventArgs e) {
            if (e.ColumnIndex == -1 && e.RowIndex > -1) {
                e.PaintBackground(e.CellBounds, true);
                using (SolidBrush br = new SolidBrush(Color.Black)) {
                    StringFormat sf = new StringFormat();
                    sf.Alignment = StringAlignment.Center;
                    sf.LineAlignment = StringAlignment.Center;
                    e.Graphics.DrawString(e.RowIndex.ToString(),
                    e.CellStyle.Font, br, e.CellBounds, sf);
                }
                e.Handled = true;
            }
        }

        private void buttonsEnable(bool enable) {
            generateButton.Enabled = enable;
            shiftButton.Enabled = enable;
            toSolveButton.Enabled = enable;
        }
    }
}
