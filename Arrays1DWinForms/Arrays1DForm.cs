﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Arrays1DWinForms {

    public partial class Arrays1DForm : Form {

        const double MIN = -10.51;
        const double MAX = 10.53;
        private double[] array;

        public Arrays1DForm() {
            InitializeComponent();
        }

        private void generateButtonClick(object sender, EventArgs e) {
            buttonsEnable(false);

            int size = (int)sizeNumericUpDown.Value;
            array = new double[size];
            Random random = new Random();

            dataGridView.RowCount = 1;
            dataGridView.ColumnCount = size;

            for (int i = 0; i < size; i++) {
                double value = random.NextDouble() * (MAX - MIN) + MIN;
                double it = Math.Round(value, 2);
                array[i] = it;
                dataGridView[i, 0].Value = it;
                dataGridView.Columns[i].HeaderText = i.ToString();
            }

            generateButton.Enabled = true;
            sumResultTextBox.Text = "";
            sortButton.Enabled = size > 0;
            toSolveButton.Enabled = size > 0;
        }

        private void sortButtonClick(object sender, EventArgs e) {
            buttonsEnable(false);

            int size = array.Length / 2;

            for (int i = 0; i < size; i++) {
                for (int j = 1; j < size - i; j++) {
                    if (array[j - 1] <= array[j]) continue;

                    double it = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = it;
                }
            }

            for (int i = 0; i < array.Length; i++) {
                dataGridView[i, 0].Value = array[i];
                dataGridView.Columns[i].HeaderText = i.ToString();
            }

            buttonsEnable(true);
        }

        private void toSolveButtonClick(object sender, EventArgs e) {
            buttonsEnable(false);

            double sum = 0;
            for (int i = 0; i < array.Length; i++) if (i > 0 && i % 3 == 0) sum += array[i];
            sumResultTextBox.Text = $"{sum:F2}";

            buttonsEnable(true);
        }

        private void buttonsEnable(bool enable) {
            generateButton.Enabled = enable;
            sortButton.Enabled = enable;
            toSolveButton.Enabled = enable;
        }
    }
}
