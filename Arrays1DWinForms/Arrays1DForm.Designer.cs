﻿namespace Arrays1DWinForms
{
    partial class Arrays1DForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Arrays1DForm));
            this.label1 = new System.Windows.Forms.Label();
            this.sizeNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.sortButton = new System.Windows.Forms.Button();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.generateButton = new System.Windows.Forms.Button();
            this.toSolveButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.sumResultTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.sizeNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(25, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(179, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "Кількість елементів";
            // 
            // sizeNumericUpDown
            // 
            this.sizeNumericUpDown.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sizeNumericUpDown.Location = new System.Drawing.Point(220, 31);
            this.sizeNumericUpDown.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.sizeNumericUpDown.Name = "sizeNumericUpDown";
            this.sizeNumericUpDown.Size = new System.Drawing.Size(120, 29);
            this.sizeNumericUpDown.TabIndex = 1;
            this.sizeNumericUpDown.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // sortButton
            // 
            this.sortButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.sortButton.AutoSize = true;
            this.sortButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.sortButton.Enabled = false;
            this.sortButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.sortButton.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sortButton.ForeColor = System.Drawing.Color.White;
            this.sortButton.Location = new System.Drawing.Point(622, 25);
            this.sortButton.Margin = new System.Windows.Forms.Padding(0, 16, 16, 16);
            this.sortButton.Name = "sortButton";
            this.sortButton.Padding = new System.Windows.Forms.Padding(8, 2, 8, 2);
            this.sortButton.Size = new System.Drawing.Size(137, 38);
            this.sortButton.TabIndex = 2;
            this.sortButton.Text = "Сортувати";
            this.sortButton.UseVisualStyleBackColor = false;
            this.sortButton.Click += new System.EventHandler(this.sortButtonClick);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.Location = new System.Drawing.Point(25, 95);
            this.dataGridView.Margin = new System.Windows.Forms.Padding(16);
            this.dataGridView.MinimumSize = new System.Drawing.Size(0, 64);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            this.dataGridView.Size = new System.Drawing.Size(734, 231);
            this.dataGridView.TabIndex = 3;
            // 
            // generateButton
            // 
            this.generateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.generateButton.AutoSize = true;
            this.generateButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.generateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generateButton.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.generateButton.ForeColor = System.Drawing.Color.White;
            this.generateButton.Location = new System.Drawing.Point(469, 25);
            this.generateButton.Margin = new System.Windows.Forms.Padding(0, 16, 16, 0);
            this.generateButton.Name = "generateButton";
            this.generateButton.Padding = new System.Windows.Forms.Padding(8, 2, 8, 2);
            this.generateButton.Size = new System.Drawing.Size(137, 38);
            this.generateButton.TabIndex = 4;
            this.generateButton.Text = "Генерувати";
            this.generateButton.UseVisualStyleBackColor = false;
            this.generateButton.Click += new System.EventHandler(this.generateButtonClick);
            // 
            // toSolveButton
            // 
            this.toSolveButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.toSolveButton.AutoSize = true;
            this.toSolveButton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.toSolveButton.Enabled = false;
            this.toSolveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toSolveButton.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toSolveButton.ForeColor = System.Drawing.Color.White;
            this.toSolveButton.Location = new System.Drawing.Point(622, 358);
            this.toSolveButton.Margin = new System.Windows.Forms.Padding(0, 16, 16, 16);
            this.toSolveButton.Name = "toSolveButton";
            this.toSolveButton.Padding = new System.Windows.Forms.Padding(8, 2, 8, 2);
            this.toSolveButton.Size = new System.Drawing.Size(137, 38);
            this.toSolveButton.TabIndex = 5;
            this.toSolveButton.Text = "Розв’язати";
            this.toSolveButton.UseVisualStyleBackColor = false;
            this.toSolveButton.Click += new System.EventHandler(this.toSolveButtonClick);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(25, 366);
            this.label2.Margin = new System.Windows.Forms.Padding(16, 16, 0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(379, 22);
            this.label2.TabIndex = 6;
            this.label2.Text = "Сума елементів, індекс яких ділиться на 3:";
            // 
            // sumResultTextBox
            // 
            this.sumResultTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.sumResultTextBox.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sumResultTextBox.Location = new System.Drawing.Point(426, 363);
            this.sumResultTextBox.Margin = new System.Windows.Forms.Padding(12, 0, 0, 0);
            this.sumResultTextBox.Name = "sumResultTextBox";
            this.sumResultTextBox.ReadOnly = true;
            this.sumResultTextBox.Size = new System.Drawing.Size(100, 29);
            this.sumResultTextBox.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(543, 414);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(238, 22);
            this.label3.TabIndex = 14;
            this.label3.Text = "Ботвін О. Ю. гр. ЗІПЗ-22-1";
            // 
            // Arrays1DForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(784, 445);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.sumResultTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.toSolveButton);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.sortButton);
            this.Controls.Add(this.sizeNumericUpDown);
            this.Controls.Add(this.label1);
            this.MinimumSize = new System.Drawing.Size(728, 344);
            this.Name = "Arrays1DForm";
            this.Text = "Лабораторна робота No3. Завдання 1";
            ((System.ComponentModel.ISupportInitialize)(this.sizeNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown sizeNumericUpDown;
        private System.Windows.Forms.Button sortButton;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.Button toSolveButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox sumResultTextBox;
        private System.Windows.Forms.Label label3;
    }
}

