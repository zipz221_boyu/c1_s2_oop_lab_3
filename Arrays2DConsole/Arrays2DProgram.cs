﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays2DConsole
{
    internal class Arrays2DProgram
    {

        const double MIN = -110.34;
        const double MAX = 110.35;
        static void Main(string[] args) {
            Console.OutputEncoding = Encoding.Unicode;
            Console.InputEncoding = Encoding.Unicode;

            double[,] matrix = initMatrix(inputValue("кількість рядків матриці"), inputValue("кількість стовпців матриці"));

            showMenu(matrix);
        }

        static int inputValue(string name) {
            Console.WriteLine($"Вкажіть {name} та натисніть Enter");

            string value = Console.ReadLine();

            if (string.IsNullOrEmpty(value)) {
                Console.WriteLine("Ви не ввели значення. Будь ласка, введіть значеня!!!\n");
                return inputValue(name);
            }

            try {
                int n = int.Parse(value);
                if (n > 0) return n;

                Console.WriteLine($"{name} не може бути меншою за 1. Будь ласка, введіть значення ще раз!!!\n");
                return inputValue(name);
            } catch (System.Exception e) {
                Console.WriteLine(
                    (e is System.FormatException ? "Помилка формату введення занчення" : "Сталася невідома помилка") +
                        ". Будь ласка, введіть значення ще раз!!!\n"
                );
                return inputValue(name);
            }

        }

        static void showMenu(double[,] matrix) {
            Console.WriteLine();
            printMenuHint("Меню");
            Console.WriteLine("Вкажіть номер пункту та натисніть Enter:");
            Console.WriteLine("\t1 -> показати сума елементів кожного рядка");
            Console.WriteLine("\t2 -> зсув парних рядків матриці зліва направо на k позицій");
            Console.WriteLine("\t3 -> показати матрицю");
            Console.WriteLine("\t0 -> завершити роботу програми");

            try {
                int it = int.Parse(Console.ReadLine());
                Console.WriteLine();
                switch (it) {
                    case 0:
                        printMenuHint("Вихід");
                        return;
                    case 1:
                        printMenuHint("показати сума елементів кожного рядка");
                        sumItemsEachRow(matrix);
                        break;
                    case 2:
                        printMenuHint("зсув парних рядків матриці зліва направо на k позицій");
                        shiftEvenItemsFromLeftToRight(matrix, inputValue("зсув парних рядків"));
                        Console.WriteLine();
                        printMatrix(matrix);
                        break;
                    case 3:
                        printMenuHint("показати матрицю");
                        printMatrix(matrix);
                        break;
                    default:
                        throw new System.FormatException();
                }
            } catch (System.Exception e) {
                Console.WriteLine("Зробіть свій вибір!!!");
            }

            showMenu(matrix);
        }

        static void printMenuHint(string name) {
            string it = $"-----{name}";
            for (int i = it.Length; i < 120; i++) it += "-";
            Console.WriteLine(it);
            Console.WriteLine();
        }

        static double[,] initMatrix(int row, int column) {
            double[,] matrix = new double[row, column];
            Random random = new Random();

            Console.Write("\n<Матриця>");
            for (int i = 0; i < row; i++) {
                Console.Write("\n\t[");
                for (int j = 0; j < column; j++) {
                    double value = random.NextDouble() * (MAX - MIN) + MIN;
                    double it = Math.Round(value, 2);
                    matrix[i, j] = it;
                    Console.Write((j > 0 ? "; " : "") + $"{it}");
                }
                Console.Write("]");
            }
            Console.WriteLine("\n</Матриця>");

            return matrix;
        }

        static void sumItemsEachRow(double[,] matrix) {
            double maxSum = 0;

            Console.WriteLine("Сума елементів кожного рядка:");
            for (int i = 0; i < matrix.GetLength(0); i++) {
                double sum = 0;
                for (int j = 0;j < matrix.GetLength(1); j++) sum = Math.Round(sum + matrix[i, j], 2);

                if (i == 0) maxSum = sum;
                else if (sum > maxSum) maxSum = sum;

                Console.WriteLine($"\tРядок {i + 1} = {sum}");
            }
            Console.WriteLine($"\nНайбільша сума: {maxSum}");
        }

        static void shiftEvenItemsFromLeftToRight(double[,] matrix, int step) {
            if (step < 1) return;

            for (int row = 0; row < matrix.GetLength(0); row++) {
                if ((row + 1) % 2 != 0) continue;

                for (int i = 0; i < step; i++) {
                    int lastIndex = matrix.GetLength(1) - 1;
                    double it = matrix[row, lastIndex];
                    for (int j = lastIndex; j > 0; j--) matrix[row, j] = matrix[row, j - 1];
                    matrix[row, 0] = it;
                }
            }
        }

        static void printMatrix(double[,] matrix) {
            Console.WriteLine("<Матриця>");
            for (int i = 0; i < matrix.GetLength(0); i++) {
                Console.Write("\t[");
                for (int j = 0; j < matrix.GetLength(1); j++) Console.Write((j > 0 ? "; " : "") + $"{matrix[i, j]}");
                Console.WriteLine("]");
            }
            Console.WriteLine("\n</Матриця>");
        }
    }
}
